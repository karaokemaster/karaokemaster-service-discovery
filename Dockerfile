FROM --platform=$BUILDPLATFORM mcr.microsoft.com/dotnet/sdk:7.0 AS build-env

# Create app directory
WORKDIR /usr/src/karaokemaster-servicediscovery

COPY . ./

RUN dotnet publish KaraokeMaster.ServiceDiscovery -f net7.0 -c Release -o out




# Runtime

FROM mcr.microsoft.com/dotnet/runtime:7.0

# Create app directory
WORKDIR /usr/src/karaokemaster-servicediscovery

# Copy assets from the build environment
COPY --from=build-env /usr/src/karaokemaster-servicediscovery/out ./

ENV COMPlus_DbgEnableMiniDump=1
ENV COMPlus_DbgMiniDumpName=/var/opt/kmdata/aa-coredump.%d

EXPOSE 5353

ENTRYPOINT [ "dotnet", "KaraokeMaster.ServiceDiscovery.dll" ]