// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using InfluxDB.Client;
using InfluxDB.Client.Api.Domain;
using InfluxDB.Client.Writes;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace KaraokeMaster.ServiceDiscovery.Metrics
{
    public class EventListener : System.Diagnostics.Tracing.EventListener
    {
        private readonly ILogger _logger;
        private readonly MetricsConfiguration _configuration;
        private readonly IEnumerable<IMetricSource> _metricSources;

        private InfluxDBClient _client;
        private WriteApi _influxWrite;
        
        public EventListener(ILoggerFactory loggerFactory, IOptionsSnapshot<MetricsConfiguration> configuration, Func<IEnumerable<IMetricSource>> metricSources)
        {
            _logger = loggerFactory.CreateLogger<EventListener>();
            _configuration = configuration.Value;
            _metricSources = metricSources();
            
            var options = new InfluxDBClientOptions.Builder()
                .Url(_configuration.InfluxEndpoint)
                .AuthenticateToken(_configuration.Token)
                .Org(_configuration.OrgId)
                .Bucket(_configuration.BucketName)
                .AddDefaultTag("module", "mdns")
                .AddDefaultTag("machine-name", Environment.MachineName)
                .Build();

            var writeOptions = new WriteOptions.Builder()
                .BatchSize(100)
                .MaxRetries(1)
                .FlushInterval(1000)
                .Build();
            
            _client = InfluxDBClientFactory.Create(options);
            _influxWrite = _client.GetWriteApi(writeOptions);

            _influxWrite.EventHandler += (sender, args) =>
            {
                if (args is WriteErrorEvent errorEvent)
                {
                    _logger.LogWarning(errorEvent.Exception, "Error writing influx events");
                }

                if (args is WriteRuntimeExceptionEvent runtimeEvent)
                {
                    _logger.LogWarning(runtimeEvent.Exception, "Runtime error writing influx events");
                }
            };

            foreach (var source in _metricSources)
            {
                source.EmitMetric += OnMetricSourceEmitMetric;
            }
        }

        private void OnMetricSourceEmitMetric(string source, string name, object value)
        {
            try
            {
                var point = PointData
                    .Measurement(source)
                    .Timestamp(DateTime.UtcNow, WritePrecision.S);

                point = value switch
                {
                    decimal valueDecimal => point.Field(name, valueDecimal),
                    double valueDouble   => point.Field(name, valueDouble),
                    float valueFloat     => point.Field(name, valueFloat),
                    ulong valueUlong     => point.Field(name, valueUlong),
                    long valueLong       => point.Field(name, valueLong),
                    uint valueUint       => point.Field(name, valueUint),
                    int valueInt         => point.Field(name, valueInt),
                    byte valueByte       => point.Field(name, valueByte),
                    _                    => point.Field(name, value.ToString())
                };

                _influxWrite.WritePoint(point);
            }
            catch (Exception ex)
            {
                _logger.LogWarning(ex, "Exception in OnMetricSourceEmitEvent handler");
            }
        }

        protected override void OnEventSourceCreated(EventSource source)
        {
            switch (source.Name)
            {
                // TODO: Allow configuration of sources/intervals
                case "System.Runtime":
                    EnableEvents(source, EventLevel.Verbose, EventKeywords.All, new Dictionary<string, string>()
                    {
                        ["EventCounterIntervalSec"] = "15"
                    });
                    break;
                case "Microsoft.AspNetCore.Hosting":
                    EnableEvents(source, EventLevel.Verbose, EventKeywords.All, new Dictionary<string, string>()
                    {
                        ["EventCounterIntervalSec"] = "5"
                    });
                    break;
                case "Microsoft.AspNetCore.Http.Connections":
                    EnableEvents(source, EventLevel.Verbose, EventKeywords.All, new Dictionary<string, string>()
                    {
                        ["EventCounterIntervalSec"] = "5"
                    });
                    break;
                case "Microsoft-AspNetCore-Server-Kestrel":
                case "Microsoft.AspNetCore.Server.Kestrel":
                    EnableEvents(source, EventLevel.Verbose, EventKeywords.All, new Dictionary<string, string>()
                    {
                        ["EventCounterIntervalSec"] = "5"
                    });
                    break;
                case "System.Net.Http":
                    EnableEvents(source, EventLevel.Verbose, EventKeywords.All, new Dictionary<string, string>()
                    {
                        ["EventCounterIntervalSec"] = "10"
                    });
                    break;
                case "System.Net.NameResolution":
                    EnableEvents(source, EventLevel.Verbose, EventKeywords.All, new Dictionary<string, string>()
                    {
                        ["EventCounterIntervalSec"] = "60"
                    });
                    break;
                case "System.Net.Security":
                    EnableEvents(source, EventLevel.Verbose, EventKeywords.All, new Dictionary<string, string>()
                    {
                        ["EventCounterIntervalSec"] = "60"
                    });
                    break;
                case "System.Net.Sockets":
                    EnableEvents(source, EventLevel.Verbose, EventKeywords.All, new Dictionary<string, string>()
                    {
                        ["EventCounterIntervalSec"] = "60"
                    });
                    break;
                default:
                    return;
            }
        }

        protected override void OnEventWritten(EventWrittenEventArgs eventData)
        {
            try
            {
                if (!eventData.EventName.Equals("EventCounters"))
                {
                    return;
                }

                var source = eventData.EventSource.Name.ToLower().Replace(".", "_");
                var metrics = new List<PointData>();

                for (int i = 0; i < eventData.Payload.Count; ++i)
                {
                    if (eventData.Payload[i] is IDictionary<string, object> eventPayload)
                    {
                        var point = GetMetricPoint(source, eventPayload);

                        if (point != null)
                            metrics.Add(point);
                    }
                }

                if (metrics.Count > 0)
                    _influxWrite.WritePoints(metrics);
            }
            catch (Exception ex)
            {
                _logger.LogWarning(ex, "Exception in EventWritten handler");
            }
        }

        public override void Dispose()
        {
            try
            {
                _influxWrite.Dispose();
                _client.Dispose();
            }
            catch {}

            base.Dispose();
        }

        private PointData GetMetricPoint(string source, IDictionary<string, object> eventPayload)
        {
            try
            {
                if (!eventPayload.TryGetValue("Name", out object displayValue))
                    return null;
                
                var counterName = displayValue.ToString();

                var point = PointData.Measurement(source)
                        .Timestamp(DateTime.UtcNow, WritePrecision.Ns)
                    ;

                if (eventPayload.TryGetValue("Mean", out object value) ||
                    eventPayload.TryGetValue("Increment", out value))
                {
                    point = value switch
                    {
                        decimal valueDecimal => point.Field(counterName, valueDecimal),
                        double valueDouble   => point.Field(counterName, valueDouble),
                        float valueFloat     => point.Field(counterName, valueFloat),
                        ulong valueUlong     => point.Field(counterName, valueUlong),
                        long valueLong       => point.Field(counterName, valueLong),
                        uint valueUint       => point.Field(counterName, valueUint),
                        int valueInt         => point.Field(counterName, valueInt),
                        byte valueByte       => point.Field(counterName, valueByte),
                        _                    => point.Field(counterName, value.ToString())
                    };
                }

                return point;
            }
            catch (Exception ex)
            {
                _logger.LogWarning(ex, "Exception creating metric point");
                return null;
            }
        }
    }
}