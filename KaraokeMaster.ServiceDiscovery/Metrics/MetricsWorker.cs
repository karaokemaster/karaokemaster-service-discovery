// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace KaraokeMaster.ServiceDiscovery.Metrics
{
    public class MetricsWorker : IHostedService, IDisposable
    {
        private readonly ILogger _logger;
        private readonly IOptionsMonitor<MetricsConfiguration> _metricsConfiguration;
        private readonly Func<EventListener> _eventListenerFactory;

        private IDisposable _configListener;
        private IDisposable _eventListener;

        public MetricsWorker(ILoggerFactory loggerFactory, IOptionsMonitor<MetricsConfiguration> metricsConfiguration, Func<EventListener> eventListenerFactory)
        {
            _logger = loggerFactory.CreateLogger<MetricsWorker>();
            _metricsConfiguration = metricsConfiguration;
            _eventListenerFactory = eventListenerFactory;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Starting metrics service");

            Start();

            _configListener = _metricsConfiguration.OnChange(OnConfigurationChange);
            
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Stopping metrics service");

            _configListener?.Dispose();
            _configListener = null;

            Stop();

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _configListener?.Dispose();
        }
        
        private void Start()
        {
            if (!_metricsConfiguration.CurrentValue.Enabled) return;
            
            bool configValid = true;

            if (!Uri.TryCreate(_metricsConfiguration.CurrentValue.InfluxEndpoint, UriKind.Absolute, out var endpointUri))
            {
                _logger.LogWarning("Invalid metrics configuration - missing or invalid InfluxEndpoint");
                configValid = false;
            }

            if (string.IsNullOrWhiteSpace(_metricsConfiguration.CurrentValue.Token))
            {
                _logger.LogWarning("Invalid metrics configuration - missing or invalid Token");
                configValid = false;
            }

            if (string.IsNullOrWhiteSpace(_metricsConfiguration.CurrentValue.OrgId))
            {
                _logger.LogWarning("Invalid metrics configuration - missing or invalid OrgId");
                configValid = false;
            }

            if (string.IsNullOrWhiteSpace(_metricsConfiguration.CurrentValue.BucketName))
            {
                _logger.LogWarning("Invalid metrics configuration - missing or invalid BucketName");
                configValid = false;
            }

            if (!configValid)
                return;
            
            _logger.LogDebug("Starting metrics publish to InfluxDB {Uri}", endpointUri);

            _eventListener = _eventListenerFactory();
        }

        private void Stop()
        {
            _eventListener.Dispose();
            _eventListener = null;
        }
        
        private void OnConfigurationChange(MetricsConfiguration configuration)
        {
            // TODO: Don't restart if nothing changed            
            _logger.LogInformation("Configuration changed; restarting...");

            Stop();
            Start();
        }
    }
}