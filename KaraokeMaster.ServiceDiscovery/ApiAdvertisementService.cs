// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Makaretu.Dns;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace KaraokeMaster.ServiceDiscovery
{
    public class ApiAdvertisementService : BackgroundService
    {
        private readonly ILogger<ApiAdvertisementService> _logger;
        private readonly IConfiguration _configuration;

        private MulticastService _mDns;

        public ApiAdvertisementService(ILoggerFactory loggerFactory, IConfiguration configuration)
        {
            _logger = loggerFactory.CreateLogger<ApiAdvertisementService>();
            _configuration = configuration;
            
            SetupMDns();
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.Register(StopAdvertisement);

            return Task.Run(StartAdvertisement, stoppingToken);
        }

        private void SetupMDns()
        {
            _mDns = new MulticastService();

            MulticastService
                .GetIPAddresses()
                .ToList()
                .ForEach(a => _logger.LogDebug($"[mDNS] IP Address {a}"));

            _mDns.QueryReceived += (s, e) =>
            {
                var names = e
                    .Message
                    .Questions
                    .Select(q => $"{q.Name} {q.Type}");
                
                _logger.LogTrace($"[mDNS] got a query for {string.Join(", ", names)}");
            };
            _mDns.AnswerReceived += (s, e) =>
            {
                var names = e
                    .Message
                    .Answers
                    .Select(q => $"{q.Name} {q.Type}")
                    .Distinct();
                
                _logger.LogTrace($"[mDNS] got answer for {string.Join(", ", names)}");
            };
            
            _mDns.NetworkInterfaceDiscovered += (s, e) =>
            {
                foreach (var nic in e.NetworkInterfaces)
                {
                    _logger.LogDebug($"[mDNS] discovered NIC '{nic.Name}'");
                }
            };

            // TODO: Allow configuring instance name and/or use WiFi SSID
            var baseUrl = _configuration.GetValue<string>("PublicUrl");
            var uri = new Uri(baseUrl);

            var lokiUrl = _configuration.GetValue<string>("Logging:Loki:Http:Address");
            
            var apiAdvertisement = new ServiceProfile(uri.Host, "_karaokemaster._tcp", (ushort)uri.Port);
            // TODO: Load capabilities from API...
            // TODO: Add subtype for beat detection, etc.
            apiAdvertisement.Subtypes.Add("apiv1");
            
            // TODO: Retrieve additional configuration from API
            // TODO: Watch for changes
            // TODO: (optional) separate config parameter for broadcast url
            var baseUri = new Uri(baseUrl);
            apiAdvertisement.AddProperty("url", baseUrl);
            apiAdvertisement.AddProperty("api", new Uri(baseUri, "/api").ToString());
            apiAdvertisement.AddProperty("kiosk", new Uri(baseUri, "/api/hubs/kiosk").ToString());
            apiAdvertisement.AddProperty("kioskupdates", new Uri(baseUri, "/updates/kiosk").ToString());
            apiAdvertisement.AddProperty("loki", lokiUrl);

            var sd = new Makaretu.Dns.ServiceDiscovery(_mDns);
            sd.Advertise(apiAdvertisement);
        }

        private void StartAdvertisement()
        {
            _logger.LogInformation("Starting mDNS advertisement");
            _mDns.Start();
        }

        private void StopAdvertisement()
        {
            _mDns.Stop();
            _logger.LogInformation("Stopped mDNS advertisement");
        }
    }
}