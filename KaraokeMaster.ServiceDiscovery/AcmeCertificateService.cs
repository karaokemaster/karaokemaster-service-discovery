// Copyright 2019-2023 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;
using Certes;
using Certes.Acme;
using Certes.Acme.Resource;
using CloudFlare.Client;
using CloudFlare.Client.Api.Zones.DnsRecord;
using CloudFlare.Client.Enumerators;
using KaraokeMaster.ServiceDiscovery.Metrics;
using Microsoft.Azure.Devices;
using Microsoft.Azure.Devices.Client;
using Microsoft.Azure.Devices.Shared;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace KaraokeMaster.ServiceDiscovery
{
    public class AcmeCertificateService : BackgroundService, IMetricSource
    {
        private const int MaxValidationRetries = 3;
        private const int MaxStatusAttempts = 100;
        
        private readonly ILogger<AcmeCertificateService> _logger;
        private readonly ModuleClient _moduleClient;
        private readonly IOptionsMonitor<AcmeCaConfiguration> _acmeCaConfiguration;
        private readonly IOptionsMonitor<CloudFlareConfiguration> _cloudFlareConfiguration;
        private readonly IOptionsMonitor<Configuration> _configuration;
        private CancellationTokenSource _configChangeTokenSource;
        private static SemaphoreSlim _certSemaphore = new SemaphoreSlim(1, 1);
        private DateTime _certIssued = DateTime.MinValue;
        private DateTime _certExpiration = DateTime.MinValue;
        private DateTime _certRenewal = DateTime.MinValue;
        
        public event EmitMetricEventArgs EmitMetric;

        public AcmeCertificateService(ILoggerFactory loggerFactory, ModuleClient moduleClient, IOptionsMonitor<AcmeCaConfiguration> acmeCaConfiguration, IOptionsMonitor<CloudFlareConfiguration> cloudFlareConfiguration, IOptionsMonitor<Configuration> configuration)
        {
            _logger = loggerFactory.CreateLogger<AcmeCertificateService>();
            _moduleClient = moduleClient;
            _acmeCaConfiguration = acmeCaConfiguration;
            _cloudFlareConfiguration = cloudFlareConfiguration;
            _configuration = configuration;

            // TODO: These both get called for any change (currently), so we should probably debounce that
            _acmeCaConfiguration.OnChange(_ => OnConfigurationChange());
            _cloudFlareConfiguration.OnChange(_ => OnConfigurationChange());
        }
        
        public AcmeCertificateService(ILoggerFactory loggerFactory, IOptionsMonitor<AcmeCaConfiguration> acmeCaConfiguration, IOptionsMonitor<CloudFlareConfiguration> cloudFlareConfiguration, IOptionsMonitor<Configuration> configuration)
            : this(loggerFactory, null, acmeCaConfiguration, cloudFlareConfiguration, configuration)
        {
        }
        
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    _configChangeTokenSource = new CancellationTokenSource();
                    var token = CancellationTokenSource.CreateLinkedTokenSource(new[]
                        {_configChangeTokenSource.Token, stoppingToken}).Token;

                    var dueAt = await CheckCertificateExpiration();
                    
                    if (dueAt == DateTime.MinValue)
                    {
                        var retry = TimeSpan.FromMinutes(5);
                        _logger.LogInformation("Unable to check certificate expiration; retrying in {Retry}", retry);

                        await Task.Delay(retry, token);
                        continue;
                    }

                    var delay = dueAt - DateTime.Now;
                    if (delay < TimeSpan.Zero) delay = TimeSpan.Zero;

                    _logger.LogInformation("Certificate renewal due at {DueAt}, renewing in {Delay}", dueAt, delay);

                    if (delay > TimeSpan.FromHours(1))
                    {
                        // TODO: Republish cert stats sooner, so that Grafana doesn't time them out...
                        // TODO: Do this from a constant loop, with a lock on one (or more) of the member variables...
                        // await ReportCertificateDetails(_certIssued, _certExpiration, _certRenewal);

                        await Task.Delay(TimeSpan.FromHours(1), token);
                        continue;
                    }
                    
                    await Task.Delay(delay, token);

                    await RenewCertificate();

                    await ReloadWebServer();
                }
                catch (OperationCanceledException)
                {}
                catch (Exception e)
                {
                    _logger.LogError(e, "Exception in ACME Certificate Service");
                    throw;
                }
                finally
                {
                    _configChangeTokenSource.Dispose();

                    await Task.Delay(TimeSpan.FromMinutes(5), stoppingToken);
                }
            }
        }

        private void OnConfigurationChange()
        {
            _logger.LogDebug("Configuration changed; restarting certificate watcher...");
            
            _configChangeTokenSource.Cancel();
        }
        
        private async Task<DateTime> CheckCertificateExpiration()
        {
            var fqdns = _acmeCaConfiguration.CurrentValue.Fqdns;

            // TODO: Maybe we should do this for all FQDNs?
            // Or, arguably, use PublicUri config value?
            var uri = new UriBuilder("https", fqdns.First()).ToString();
            var certificate = await GetServerCertificate(uri);

            if (certificate == null) return DateTime.MinValue;

            try
            {
                await _certSemaphore.WaitAsync();
                
                _certExpiration = certificate.NotAfter;
                _certIssued = certificate.NotBefore;
                var duration = _certExpiration - _certIssued;
                var expiresIn = _certExpiration - DateTime.UtcNow;
                _certRenewal = _certIssued + (duration / 1.5);

                // Make sure we catch renewals for really short duration (temporary) certs
                if (_certExpiration - DateTime.Now < TimeSpan.FromDays(3))
                    _certRenewal = DateTime.Now.AddMinutes(-5);

                // TODO: Move to ReportCertificateDetails?
                EmitMetric?.Invoke("AcmeCertificateService", "expiresIn", expiresIn.TotalDays);

                _logger.LogDebug("Certificate valid from {Issued} to {Expiration}; should renew after {Renewal}",
                    _certIssued, _certExpiration, _certRenewal);
            }
            finally
            {
                _certSemaphore.Release();
            }

            await ReportCertificateDetails();
            
            return _certRenewal;
        }

        private async Task RenewCertificate()
        {
            var config = _acmeCaConfiguration.CurrentValue;
            var server = config.Server;
            Uri uri = typeof(WellKnownServers).GetProperty(server)?.GetValue(null) as Uri
                ?? new Uri(server);
            
            _logger.LogInformation("Using ACME server {Uri}", uri);

            var accountKey = KeyFactory.FromPem(config.AccountKey);
            // TODO: Generate key if not exists
            // accountKey = KeyFactory.NewKey(KeyAlgorithm.ES256);
            var acme = new AcmeContext(uri, accountKey);
            // TODO: Create account if key not exists
            // var account = await acme.NewAccount("mike@atomicmike.com", true);
            
            var order = await acme.NewOrder(config.Fqdns);
            var auths = await order.Authorizations();

            await Task.WhenAll(auths.ToList().Select(a => ProcessDnsAuthorization(acme, a)));
            
            try
            {
                var privateKey = KeyFactory.NewKey(KeyAlgorithm.ES256);

                // TODO: Allow customization of certificate details
                var cert = await order.Generate(new CsrInfo
                {
                    CountryName = "US",
                    State = "Ohio",
                    Locality = "Cincinnati",
                    Organization = "KaraokeMaster",
                    OrganizationUnit = "API",
                    CommonName = config.Fqdns.First(),
                }, privateKey);

                var keyPem = privateKey.ToPem();
                var certPem = cert.ToPem();

                var keyPath = Path.Join(_acmeCaConfiguration.CurrentValue.OutputPath, "karaokemaster.key");
                var chainPath = Path.Join(_acmeCaConfiguration.CurrentValue.OutputPath, "karaokemaster.chained.crt");
                
                await File.WriteAllTextAsync(keyPath, keyPem);
                await File.WriteAllTextAsync(chainPath, certPem);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error processing certificate renewal");
                return;
            }
            
            await ReloadWebServer();

            // TODO: Verify certificate installation
            // TODO: Report status via module properties/messages
            await CheckCertificateExpiration();
            
            await Task.CompletedTask;
        }
        
        private Task<X509Certificate2> GetServerCertificate(string url)
        {
            var tcs = new TaskCompletionSource<X509Certificate2>();
            
            X509Certificate2 certificate = null;
            var httpClientHandler = new HttpClientHandler
            {
                ServerCertificateCustomValidationCallback = (_, cert, __, ___) =>
                {
                    certificate = new X509Certificate2(cert.GetRawCertData());
                    tcs.SetResult(certificate);
                    return true;
                }
            };

            try
            {
                var httpClient = new HttpClient(httpClientHandler);
                httpClient.Send(new HttpRequestMessage(HttpMethod.Head, url));
            }
            catch (HttpRequestException ex)
            {
                _logger.LogWarning(ex, "Error retrieving current certificate");
                tcs.SetResult(null);
            }

            return tcs.Task;
        }

        private async Task<DnsRecord> CreateDnsTxtRecord(string name, string txt)
        {
            using var cfClient = new CloudFlareClient(_cloudFlareConfiguration.CurrentValue.Email,
                _cloudFlareConfiguration.CurrentValue.Key);
            var zones = await cfClient.Zones.GetAsync();
            var zone = zones.Result.First(z => name.EndsWith(z.Name));
                
            _logger.LogDebug("Creating DNS TXT record for {Name}", name);
                
            var dnsRecord = await cfClient.Zones.DnsRecords.AddAsync(zone.Id, 
                new NewDnsRecord { Type = DnsRecordType.Txt, Name = name, Content = txt });

            await RemoveOrphanDnsTxtRecords(dnsRecord.Result);

            return dnsRecord.Result;
        }

        private async Task RemoveOrphanDnsTxtRecords(DnsRecord dnsRecord)
        {
            using var cfClient = new CloudFlareClient(_cloudFlareConfiguration.CurrentValue.Email,
                _cloudFlareConfiguration.CurrentValue.Key);
            var zones = await cfClient.Zones.GetAsync();
            var zone = zones.Result.First(z => z.Id == dnsRecord.ZoneId);

            var name = dnsRecord.Name;
            
            var records = await cfClient.Zones.DnsRecords.GetAsync(zone.Id);
            var orphans = records.Result.Where(r => r.Name == name && r.Id != dnsRecord.Id).ToList();

            _logger.LogDebug("Found {Count} orphan records for {Name}; removing...", orphans.Count, name);

            try
            {
                var orphanTasks = orphans.Select(RemoveDnsRecord).ToArray();
                Task.WaitAll(orphanTasks);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error in RemoveOrphanDnsTxtRecords");
            }
        }

        private async Task RemoveDnsRecord(DnsRecord record)
        {
            using var cfClient = new CloudFlareClient(_cloudFlareConfiguration.CurrentValue.Email,
                _cloudFlareConfiguration.CurrentValue.Key);
            var zones = await cfClient.Zones.GetAsync();
            var zone = zones.Result.First(z => z.Id == record.ZoneId);
                
            _logger.LogDebug("Removing DNS TXT record for {DnsName}", record.Name);

            await cfClient.Zones.DnsRecords.DeleteAsync(zone.Id, record.Id);
        }

        private async Task ProcessDnsAuthorization(AcmeContext acme, IAuthorizationContext auth, int retries = MaxValidationRetries)
        {
            var dnsChallenge = await auth.Dns();
            var dnsName = (await auth.Resource()).Identifier.Value;
            var challengeName = $"_acme-challenge.{dnsName}";
            _logger.LogInformation("Performing DNS validation for FQDN {DnsName}", dnsName);
            
            var dnsTxt = acme.AccountKey.DnsTxt(dnsChallenge.Token);
            var dnsRecord = await CreateDnsTxtRecord(challengeName, dnsTxt);

            try
            {
                // TODO: Do our own DNS query before we request validation
                
                _logger.LogDebug("Awaiting DNS validation for {DnsName}", challengeName);
                await Task.Delay(TimeSpan.FromSeconds(10));

                var attempts = MaxStatusAttempts;
                var result = await dnsChallenge.Validate();

                while (attempts > 0 && result.Status is ChallengeStatus.Pending or ChallengeStatus.Processing)
                {
                    attempts--;

                    _logger.LogDebug("Validation for {DnsName} not complete (status: {Status}); waiting 30s... ({RemainingAttempts}/{MaxAttempts})", challengeName, result.Status, attempts, MaxStatusAttempts);
                    if (result.Error != null)
                        _logger.LogTrace("ACME Error: {AcmeError}", result.Error?.ToString());
                    await Task.Delay(TimeSpan.FromSeconds(30));

                    result = await dnsChallenge.Resource();
                }

                if (result.Status == ChallengeStatus.Invalid)
                {
                    _logger.LogDebug("Validation failed for {DnsName} (status: {Status})", challengeName, result.Status);

                    _logger.LogDebug("Removing existing DNS record for {DnsName}...", challengeName);
                    await RemoveDnsRecord(dnsRecord);

                    if (retries < 1)
                    {
                        _logger.LogDebug("Aborting validation for {DnsName}", challengeName);
                        return;
                    }
                    
                    _logger.LogDebug("Restarting validation for {DnsName} (status: {Status}); waiting 30s... ({Retries}/{MaxRetries})", challengeName, result.Status, retries, MaxValidationRetries);
                    await Task.Delay(TimeSpan.FromSeconds(30));

                    await ProcessDnsAuthorization(acme, auth, retries - 1);
                    return;
                }
                
                _logger.LogDebug("DNS validation complete for {DnsName}", challengeName);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Exception in DNS validation");
                throw;
            }
            finally
            {
                await RemoveDnsRecord(dnsRecord);
            }

            _logger.LogInformation("Validation complete for {DnsName}", dnsName);
        }

        private async Task ReloadWebServer()
        {
            try
            {
                var client = ServiceClient.CreateFromConnectionString(_configuration.CurrentValue.ServiceConnection);
                var deviceId = Environment.GetEnvironmentVariable("IOTEDGE_DEVICEID");
                // TODO: Make this configurable
                var moduleId = "webserver";
                
                var payload = $"{{\"schemaVersion\": \"1.0\", \"id\": \"{moduleId}\"}}";

                var method = new CloudToDeviceMethod("RestartModule");
                method.SetPayloadJson(payload);

                await client.OpenAsync();

                var result = await client.InvokeDeviceMethodAsync(deviceId, "$edgeAgent", method);
                _logger.LogInformation("Web server restart requested; status {Status}", result.Status);
                
                await client.CloseAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error in ReloadWebServer");
            }
        }

        private async Task ReportCertificateDetails()
        {
            if (_moduleClient == null)
            {
                _logger.LogInformation("Module client not available; skipping certificate property report");
                return;
            }

            try
            {
                await _certSemaphore.WaitAsync();
                
                if (_certIssued == DateTime.MinValue || _certExpiration == DateTime.MinValue || _certRenewal == DateTime.MinValue)
                {
                    _logger.LogInformation("Full certificate details not available; skipping certificate property report");
                    _logger.LogDebug(
                        "Current certificate details: (issued {Issued}) (expiration {Expiration}) (renewal {Renewal})",
                        _certIssued, _certExpiration, _certRenewal);
                    return;
                }

                try
                {
                    var twin = await _moduleClient.GetTwinAsync();
                    var currentProps = JsonConvert.DeserializeObject<TwinProperties>(twin.Properties.Reported.ToJson());

                    if (currentProps != null && currentProps.AcmeCertificate != null
                                             && currentProps.AcmeCertificate.Issued == _certIssued
                                             && currentProps.AcmeCertificate.Expiration == _certExpiration
                                             && currentProps.AcmeCertificate.ScheduledRenewal == _certRenewal
                       )
                    {
                        _logger.LogInformation("Certificate properties unchanged");
                        return;
                    }

                    var newProps = new TwinProperties
                        { AcmeCertificate = new AcmeCertificateTwinProperties(_certIssued, _certExpiration, _certRenewal) };
                    var props = JsonConvert.SerializeObject(newProps, new JsonSerializerSettings
                    {
                        ContractResolver = new CamelCasePropertyNamesContractResolver()
                    });

                    var properties = new TwinCollection(props);
                    await _moduleClient.UpdateReportedPropertiesAsync(properties);
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "Error writing module twin properties");
                }
            }
            finally
            {
                _certSemaphore.Release();
            }
        }

        private class TwinProperties
        {
            public AcmeCertificateTwinProperties AcmeCertificate { get; set; }
        }
        
        private class AcmeCertificateTwinProperties
        {
            public AcmeCertificateTwinProperties(DateTime issued, DateTime expiration, DateTime scheduledRenewal)
            {
                Issued = issued;
                Expiration = expiration;
                ScheduledRenewal = scheduledRenewal;
            }
            
            public DateTime Issued { get; init; }
            public DateTime Expiration { get; init; }
            public DateTime ScheduledRenewal { get; init; }
        }
    }
}