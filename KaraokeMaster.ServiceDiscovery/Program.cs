// Copyright 2019-2022 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using KaraokeMaster.ServiceDiscovery.Banner;
using KaraokeMaster.ServiceDiscovery.Metrics;
using Microsoft.Azure.Devices.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace KaraokeMaster.ServiceDiscovery
{
    public class Program
    {
        public static void Main(string[] args)
        {
            StartupBanner.Print(out var sentryRelease);

            CreateHostBuilder(sentryRelease, args).Build().Run();
        }

        private static IHostBuilder CreateHostBuilder(string sentryRelease, string[] args)
        {
            // TODO: Refactor this into a service, assuming DI can do something to help
            var isIot = !string.IsNullOrEmpty(Environment.GetEnvironmentVariable("IOTEDGE_WORKLOADURI"));
            ModuleClient moduleClient = null;
            
            if (isIot)
            {
                var moduleTask = ModuleClient.CreateFromEnvironmentAsync();
                moduleTask.Wait();
                moduleClient = moduleTask.Result;
            }
            
            return Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostContext, builder) =>
                {
                    builder.AddJsonFile(new AzureModuleConfiguration(moduleClient), "appsettings.iot.json", true, true);
                    builder.AddJsonFile("/var/opt/config/secrets.json", true, true);
                    builder.AddEnvironmentVariables();
                })
                .ConfigureLogging((hostContext, logging) =>
                {
                    logging.AddConfiguration(hostContext.Configuration.GetSection("Logging"));
                    logging.AddConsole();
                    logging.AddDebug();
                    logging.AddLoki(options =>
                    {
                        options.StaticLabels.JobName = "KaraokeMaster.mDns";
                        options.StaticLabels.IncludeInstanceLabel = true;
                        options.StaticLabels.AdditionalStaticLabels.Add("version", sentryRelease);

                        options.DynamicLabels.IncludeCategory = true;
                        options.DynamicLabels.IncludeLogLevel = true;
                        options.DynamicLabels.IncludeEventId = true;
                        options.DynamicLabels.IncludeException = false;
                    });
                })
                .UseServiceProviderFactory(new AutofacServiceProviderFactory())
                .ConfigureContainer<ContainerBuilder>(AutofacConfig.Build)
                .ConfigureServices((hostContext, services) =>
                {
                    services.Configure<Configuration>(hostContext.Configuration);
                    services.Configure<AcmeCaConfiguration>(
                        hostContext.Configuration.GetSection(AcmeCaConfiguration.ConfigurationKey));
                    services.Configure<CloudFlareConfiguration>(
                        hostContext.Configuration.GetSection(CloudFlareConfiguration.ConfigurationKey));
                    services.Configure<MetricsConfiguration>(
                        hostContext.Configuration.GetSection(MetricsConfiguration.ConfigurationKey));

                    services.AddFactory<EventListener, EventListener>();
                    
                    // services.AddHostedService<MetricsWorker>();
                    // services.AddHostedService<ApiAdvertisementService>();
                    // services.AddHostedService<AcmeCertificateService>();

                    if (moduleClient != null)
                        services.AddSingleton(moduleClient);
                });
        }
    }
}