// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using Colorful;
using Console = Colorful.Console;

namespace KaraokeMaster.ServiceDiscovery.Banner
{
    internal static class StartupBanner
    {
        public static void Print(out string release)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var gitVersionInformationType = assembly.GetType("GitVersionInformation");
            var version = gitVersionInformationType?.GetField("SemVer")?.GetValue(null)?.ToString();
            var buildHash = gitVersionInformationType?.GetField("Sha")?.GetValue(null)?.ToString();
            release = Environment.GetEnvironmentVariable("SENTRY_RELEASE") ?? $"kmmdns-{version}";

            var banner = LoadEmbeddedResourceLines("banner.txt");
            Console.WriteLineWithGradient(banner, Color.Orange, Color.DarkRed);
            
            var info = new[]
            {
                new Formatter(buildHash, Color.Silver),
                new Formatter(release, Color.Green),
            };
            
            Console.WriteLineFormatted("Build {0} (Sentry {1})", Color.Gray, info);
            
            using var fontStream = LoadEmbeddedResource("smslant.flf");
            var font = FigletFont.Load(fontStream);
            var figlet = new Figlet(font);

            Console.WriteLine(figlet.ToAscii($"Welcome to v{version}"), Color.Green);
        }

        private static Stream LoadEmbeddedResource(string filename)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var stream = assembly.GetManifestResourceStream(typeof(StartupBanner), filename);

            return stream;
        }
        
        private static string[] LoadEmbeddedResourceLines(string filename)
        {
            try
            {
                using var stream = LoadEmbeddedResource(filename);
                if (stream == null)
                    return null;
                
                using var reader = new StreamReader(stream);
                var lines = new List<string>();
                string line;

                while ((line = reader.ReadLine()) != null)
                {
                    lines.Add(line);
                }
                return lines.ToArray();
            }
            catch
            {
                // ignored
            }

            return null;
        }    
    }
}